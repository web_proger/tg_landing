$(document).ready(function(){	
	
	//*****************************************
	//On Scroll Functionality
	$(window).scroll( () => {
		var windowTop = $(window).scrollTop();
		windowTop > 0 ? $('nav').addClass('navShadow') : $('nav').removeClass('navShadow');
		windowTop > 0 ? $('ul').css('top','0px') : $('ul').css('top','0px');
	});
	
	//Click Logo To Scroll To Top
	$('#logo').on('click', () => {
		$('html,body').animate({
			scrollTop: 0
		},500);
	});
	
	//Smooth Scrolling Using Navigation Menu
	$('a[href*="#"]').on('click', function(e){
		$('html,body').animate({
			scrollTop: $($(this).attr('href')).offset().top
		},500);
		e.preventDefault();
	});
	
	//Toggle Menu
	$('#menu-toggle').on('click', () => {
		$('#menu-toggle').toggleClass('closeMenu');
		$('ul').toggleClass('showMenu');
		
		$('li').on('click', () => {
			$('ul').removeClass('showMenu');
			$('#menu-toggle').removeClass('closeMenu');
		});
	});
	
	//*****************************************
	
	// маска
	$('input[name="phone"]').mask("+7 (999) 999-99-99");
	
	// Поп-ап
	$('.popup').fancybox({
		padding : 0,
		helpers: {
			overlay: {
				locked: false
			}
		}
	});
	
	
	//Карусель
	/*if ($('.owl').length>0){
		var owl = $('.owl');
		owl.owlCarousel({
			margin: 45,
			nav: false,
			dots: true,
			loop: true,
			responsive: {
			  0: {
				items: 1
			  },
			  480: {
				items: 2
			  },
			  660: {
				items: 3
			  },
			  880: {
				items: 2
			  },
			  1450: {
				items: 3
			  }
			}
		});
	}*/
	
		 
	// Отключаем все формы
	$('form').submit(function() {
		//return false;
	});	
	
	// Проверка на правильность е-майла
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		return pattern.test(emailAddress);
	}
	
	//Отправка на почту
	function SEND_mail(s_name, s_phone, s_email, s_comment, s_product, s_temp, s_subject, s_text, s_url_send, s_view_name, s_view_phone, s_view_mail, s_view_comment, s_view_product, s_view_temp){
		
		// Имя
		if (s_name == 'no_name'){
		   var name = s_name;
		   var real_name = false;		
		} else{
		   var name = $(s_name).val();
		   var real_name = true;				
		}	
		
		// Телефон
		if (s_phone == 'no_phone'){
		   var phone = s_phone;
		   var real_phone = false;		
		} else{
		   var phone = $(s_phone).val();
		   var real_phone = true;				
		}
		
		// Почта
		if(s_email.indexOf('@') + 1) {
		   var email = s_email;
		   var real_mail = false;
		}else{
		   var email = $(s_email).val();
		   var real_mail = true;
		}
		
		// Комментарий
		if (s_comment == 'no_comment'){
		   var comment = s_comment;
		   var real_comment = false;		
		} else{
		   var comment = $(s_comment).val();
		   var real_comment = true;				
		}
		
		// Продукт
		if (s_product == 'no_product'){
		   var product = s_product;
		   var real_product = false;		
		} else{
		   var product = $(s_product).val();
		   var real_product = true;				
		}
		
		// Доп поле
		if (s_temp == 'no_temp'){
		   var temp = s_temp;
		   var real_temp = false;		
		} else{
		   var temp = $(s_temp).val();
		   var real_temp = true;				
		}
		
		var subject = s_subject;
		var text = s_text;
		
		var view_name = s_view_name;
		var view_phone = s_view_phone;
		var view_mail = s_view_mail;
		var view_comment = s_view_comment;
		var view_product = s_view_product;
		var view_temp = s_view_temp;
		
		if(name == '' || phone == '' || email == ''){alert('Заполните все поля.');}
		else if (view_comment == 'yes_faq' && comment == '') {alert('Укажите вопрос.');}
		else if (isValidEmailAddress(email))
		{ 
			$.ajax({
			 type: "POST",
			 url: s_url_send,
			 cache: false,
			 data: "name="+name+"&phone="+phone+"&email="+email+"&comment="+comment+"&product="+product+"&temp="+temp+"&subject="+subject+"&text="+text+"&view_name="+view_name+"&view_phone="+view_phone+"&view_mail="+view_mail+"&view_comment="+view_comment+"&view_product="+view_product+"&view_temp="+view_temp,
			 success: function(answ){
				if (real_name == true) $(s_name).val('');
				if (real_phone == true) $(s_phone).val('');
				if (real_mail == true) $(s_email).val('');
				if (real_comment == true) $(s_comment).val('');
				if (real_product == true) $(s_product).val('');
				if (real_temp == true) $(s_temp).val('');

				
				$( ".fancybox-close" ).trigger( "click" );
				setTimeout( function() { 
					window.location = "/spasibo";
				} , 501);
				/*setTimeout( function() { $( ".sankyou" ).trigger( "click" ); } , 501);
				setTimeout( function() { $( ".fancybox-close" ).trigger( "click" ); } , 4000);*/
				
			 }
			});
		}
		else
		{
			alert('Неверный E-mail');
		}
	}
	
	$('.send_z').click(function(){ 
		SEND_mail(
			'#name_z', 			// id поля имя или no_name
			'#phone_z', 		// id поля телефон или no_phone
			'info@labor.ru', 			// id поля телефон или ZAKAZ@nomail.ru (можно любой другой)
			'no_comment', 		// id поля комментария или no_comment
			'no_product', 		// id поля продукта или no_product
			'#company_z', 			// id доп. поля 
			'Заказ звонка', 	// Заголовок письма
			'Заказ звонка с сайта labor.ru ', 	// Заголовок в теле письма
			'ajax/send_mail.php', 	// php-файл для отправки на почту
			'yes_name', 		// выводим в письме имя (yes_name/no_name)
			'yes_phone', 		// выводим в письме телефон (yes_phone/no_phone)
			'no_mail', 		// выводим в письме почту (yes_mail/no_mail)
			'no_comment', 		// выводим в письме комментарий (yes_comment/no_comment)
			'no_product', 		// выводим в письме продукт (yes_product/no_product)
			'yes_company' 			// выводим в письме доп поле (yes_temp/no_temp)
		); 
	});
	
	
});